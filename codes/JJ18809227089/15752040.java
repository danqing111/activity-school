/**
 * 冒泡排序函数
 * 实现冒泡排序算法对数组进行升序排序
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (a[j] > a[j + 1]) {
                // 交换元素
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
} //end
