/**  
 * 冒泡排序函数  
 * 通过不断比较相邻元素，将较大的元素交换到数组的末尾，经过n-1轮比较后，数组变得有序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制需要比较的轮数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每轮比较的次数  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
