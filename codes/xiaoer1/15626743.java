/**  
 * 冒泡排序函数  
 * 冒泡排序是一种简单的排序算法，它重复地遍历要排序的数列，一次比较两个元素，如果他们的顺序错误就把他们交换过来。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制遍历次数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环控制每一轮的比较次数  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
