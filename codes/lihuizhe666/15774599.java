/**  
 * 冒泡排序函数  
 * 将数组a按照从小到大的顺序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制所有元素都参与比较  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环负责相邻元素两两比较  
            if (a[j] > a[j + 1]) { // 如果前一个元素比后一个元素大，则交换它们  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
